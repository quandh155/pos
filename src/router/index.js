import Vue from "vue";
import Router from "vue-router";
import HomePageComponent from '@/pages/homepages'
import ProductListComponent from '@/components/product/ProductList'
import PageNotFound from '@/pages/PageNotFound'
import Login from "@/pages/login";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [

    {
      path: "",
      component: {
        render(c) {
          return c('router-view'); },
      },
      redirect: "/homepages",
      beforeEnter: (to, from, next) => {
        if (!localStorage.getItem("token")) {
          return next("/login");
        } else {
          return next();
        }
      },
      children: [
          {
              path: '/homepages',
              name: 'HomePageComponent',
              component: HomePageComponent,
              children: [
                  {
                      path: '/products-list/:id',
                      name: 'ProductListComponent',
                      component: ProductListComponent,
                  },
                  {
                    path: '/products-list',
                    name: 'ProductListComponent',
                    component: ProductListComponent,
                },
              ]
          },
      ],
    },

    {
      path: "/login",
      name: "Login",
      component: Login,
      beforeEnter: (to, from, next) => {
        if (!localStorage.getItem("token")) {
          return next();
        } else {
          return next("/");
        }
      },
    },
    {
      path: "/404",
      name: "PageNotFound",
      component: PageNotFound,
    },
    {
      path: "*",
      redirect: "/404"
    },
  ],
});

export default router;
