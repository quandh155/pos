import Vue from 'vue'
import App from './App.vue'
import store from './store'
import VueLoading from 'vue-loading-template'
import router from './router'
import interceptor from './interceptors'
import {
    Button,
    Layout,
    Icon,
    Space,
    Form,
    Select,
    Table,
    Input,
    Row,
    Col,
    Menu,
    Modal,
    FormModel,
    Tag,
    List,
    Card,
} from 'ant-design-vue';
Vue.use(Button)
Vue.use(Layout)
Vue.use(Icon)
Vue.use(Space)
Vue.use(Form)
Vue.use(Select)
Vue.use(Table)
Vue.use(Input)
Vue.use(Row)
Vue.use(Col)
Vue.use(Menu)
Vue.use(Modal)
Vue.use(FormModel)
Vue.use(Tag)
Vue.use(List)
Vue.use(Card)


interceptor()
Vue.use(VueLoading, {
    size: {
        width: '100px',
        height: '100px'
    },
    type: "bubbles",
    color: "#f18e1d"
});

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
    router,
    store
}).$mount('#app')
