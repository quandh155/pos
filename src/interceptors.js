import axios from 'axios';

export default function setup() {
    axios.interceptors.request.use(function(config) {
        config.baseURL="http://localhost:8000/api/v1"
        const token = localStorage.getItem("token")
        if (token) {
            config.headers.Authorization = `Bearer ${token}`
        }
        config.headers['X-Requested-With'] = 'XMLHttpRequest'
        return config;
    }, function(err) {
        return Promise.reject(err)
    });
    const onResponseSuccess = (response) => response;
    const onResponseError = async (err) => {
        const status = err.status || (err.response ? err.response.status : 0);
        if(status === 401){
            localStorage.removeItem("token")
            window.location.href = "/login";
        }

    }
    axios.interceptors.response.use(onResponseSuccess, onResponseError);
}