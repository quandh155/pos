export default Object.freeze({
    ADMIN_PREFIX: '/admin',
    API_PREFIX: '/api/v1',
})