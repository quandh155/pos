import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'

Vue.use(Vuex);

export default new Vuex.Store({
    modules,
    state: {
        // global state
        componentName: null,
        canClose: true, // modal
        isLoading: false,
        resetFlag: null,
        currentUser: {
            role: 0,
            name: ''
        },
        token: null,
        error: {
            isError: false,
            text: ''
        }
    },
    getters: {
        canClose: state => state.canClose,
        loggedIn: state => state.loggedIn,
        fixed: state => state.fixed,
        currentUser: state => state.currentUser,
        resetFlag: state => state.resetFlag,
    },
    mutations: {
        open(state, obj) {
            state.componentName = obj.componentName
        },
        setCanClose(state, canClose) {
            state.canClose = canClose
        },
        toggleLoginStatus(state, status) {
            state.loggedIn = status
        },
        setCurrentUser(state, metadata) {
            state.currentUser = metadata
        },
        setIsLoading(state, status) {
            state.isLoading = status
        },
        setResetFlag(state, value) {
            state.resetFlag = value
        },
    },
    actions: {
        MODAL_OPEN({commit}, obj) {
            commit('open', obj)
        },
        MODAL_CLOSE({commit}) {
            commit('close')
        },
        SET_CAN_CLOSE({commit}, canClose) {
            commit('setCanClose', canClose)
        }
    },
})
