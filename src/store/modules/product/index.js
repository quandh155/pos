import constants from './constants'
import axios from 'axios'

const state = {
    products: [],
    errorMsg: null,
};

const mutations = {
    setProducts(state, products) {
        state.products = products
    },
    setError(state, msg) {
        state.errorMsg = msg
    },
};

const getters = {
    products: state => state.products,
};

const actions = {
    async getProducts({getters, commit},categoryId) {
        console.log(categoryId,313133)
        const {data} = await axios.get(constants.GET_ALL_ITEM)
        console.log(2132131,this.$route)
        if (data) {
            commit('setProducts', data.data)
            return getters.products.data
        }

        commit('setError', data.errorMessage)
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
