import constants from './constants'
import axios from 'axios'

const state = {
    categories: [],
    query: null,
    errorMsg: null,
};

const mutations = {
    setCategories(state, categories) {
        state.categories = categories
    },
    setError(state, msg) {
        state.errorMsg = msg
    },
};

const getters = {
    categories: state => state.categories,
};


const actions = {
    async getCategory({getters, commit}) {
        const {data} = await axios.get(constants.GET_CATEGORIES)
        
        if (data) {
            commit('setCategories', data.data)
            return getters.categories.data
        }

        commit('setError', data.errorMessage)
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
