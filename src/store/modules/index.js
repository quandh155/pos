import camelCase from 'lodash/camelCase'

const requireModule = require.context('.', true, /\.js$/);
const modules = {};

requireModule.keys().forEach(fileName => {
    if(fileName.indexOf('constants.js') !== -1 || fileName === "./index.js") return;

    const moduleName = camelCase(
        fileName.replace(/(\.\/|index\.js)/g, '')
    );
    modules[moduleName] = requireModule(fileName).default
});

export default modules