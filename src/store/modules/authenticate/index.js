import constants from "./constants";
import axios from "axios";
const state = {
  user: "abc",
  role: 0,
  loginError:""
};

const mutations = {
  setUser(state, user) {
    state.user = user;
  },
  setRole(state, role) {
    state.role = role;
  },
  setLoginError(state, msg) {
    state.loginError = msg;
  },
  // setResponseDataRegister(state, response) {
  //     state.userRole = response
  // },
};

const getters = {
  user: (state) => {
    return state.user;
  },
  role: (state) => {
    return state.role;
  },
  loginError: (state) => {
    return state.loginError;
  },
};

const actions = {
  async adminLogin({ getters, commit }, dataRegister) {
     const response = await axios.post(constants.SIGNIN_COMPLETE_ENDPOINT, dataRegister)
    if(response && response.data){
      localStorage.setItem("token", response.data.token);
      localStorage.setItem(
        "currentUser",
        JSON.stringify(response.data.data)
      );
      commit("setUser", response.data.data);
    return getters.user;

    }else {
      commit("setIsLoading", false, { root: true });
    return null;

    }

  },
  async getRole({ getters, commit }) {
    await axios
      .get(constants.ROLE_COMPLETE_ENDPOINT)
      .then((response) => {
        commit("setRole", response.data.role);
      })
      .catch((e) => {
        console.log(e);
        commit("setIsLoading", false, { root: true });
      });
    return getters.role;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
