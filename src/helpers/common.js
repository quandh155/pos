const modules = {
    numberWithCommas: (x) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    },

    hasNull: (target) => {
        for (const member in target) {
            if (target[member] == null)
                return true;
        }
        return false;
    },

    getDecodedURI: (paramKey, raw) => {
        let url = encodeURI(window.location.href), result;
        url = new URL(url);
        if (raw) {
            result = url.searchParams.get(encodeURI(paramKey));
        } else {
            result = decodeURIComponent(url.searchParams.get(encodeURI(paramKey)));
        }

        return result === 'null' ? '' : result
    },

    calculateDays(currentMonth, currentYear) {
        let t = 0;
        let m = [];
        let d = new Date(currentYear, currentMonth, 0);
        for (let i = 0; i < 6; i++) {
            d.setMonth(d.getMonth() + 1);
            m.push({
                month: d.getMonth(),
                year: d.getFullYear()
            });
        }
        for (let i in m) {
            t += this.daysInMonth(m[i].month, m[i].year)
        }
        return t;
    },

    daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    },

    calculateWeeks: (a, b) => {
        let diff = (a.getTime() - b.getTime()) / 1000;
        diff /= (60 * 60 * 24 * 7);
        return Math.abs(Math.round(diff));
    },

    fullWidthConvert: (fullWidth) => {
        return fullWidth.replace(/[\uFF00-\uFFFE]/g, function (ch) {
                return String.fromCharCode(ch.charCodeAt(0) - 0xfee0);
            }
        );
    },

    toASCII(chars) {
        var ascii = '';
        for (var i = 0, l = chars.length; i < l; i++) {
            var c = chars[i].charCodeAt(0);

            // make sure we only convert half-full width char
            if (c >= 0xFF00 && c <= 0xFFEF) {
                c = 0xFF & (c + 0x20);
            }

            ascii += String.fromCharCode(c);
        }

        return ascii;
    },

    escapeControlCharacter: (str) => {
        return str.replace(/\\t/g, "")
            .replace(/\\n/g, "\\n")
            .replace(/\\'/g, "\\'")
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, "\\&")
            .replace(/\\r/g, "\\r")
            .replace(/\\b/g, "\\b")
            .replace(/\\f/g, "\\f")
            .replace(/\\ /g, "\\ ");
    },

    generateFormPayload: (except, currentPointer) => {
        const payload = {};
        let oldData = {};
        currentPointer.formDatas.map(item => {
            if (!except.includes(item.id) && item.id !== undefined) {
                if (['text', 'email', 'password', 'number', 'tel'].includes(item.type)) {
                    payload[item.id] = oldData[item.id] = document.getElementById(item.id).value
                } else if (item.type === 'textarea') {
                    payload[item.id] = item.items[0].value || ''
                } else if (item.type === 'select') {
                    payload[item.id] = item.selected
                }
                // radio/checkbox, multiple input, ... left
            }
        });

        currentPointer.$store.commit('setOldInput', oldData);
        return payload;
    },

    prefixCheck: (prefix, str) => {
        if (str.charAt(0) !== prefix) {
            return prefix + str
        } else {
            return str
        }
    },

    getFieldForMaster(currentPointer, except) {
        let arr = currentPointer.formDatas.filter(item => {
            return ['text', 'area', 'tel', 'password', 'number', 'email', 'textarea'].includes(item.type) && !except.includes(item.id)
        });

        arr = arr.map(item => {
            return item.id
        });

        return arr;
    },

    convertBytesToMegabytes(bytes) {
        return bytes / (1024 * 1024)
    },

    insertStringAtPosition(source, addition, index) {
        if (source) {
            return [source.slice(0, index), addition, source.slice(index)].join('')
        } else {
            return ''
        }

    },

    checkDateIsPastOrNow(date) {
        const now = new Date(new Date().toString());
        const d = new Date(date);
        return d <= now
    },

    convertDateObjectToString(dateObj) {
        let m = dateObj.getMonth() + 1;
        if (m < 10) {
            m = '0' + m
        }
        return dateObj.getFullYear() + '-' + m + '-' + dateObj.getDate();
    },

    checkBrowser(browser) {
        const ua = window.navigator.userAgent.toLowerCase(); //Check the userAgent property of the window.navigator object
        // const msie = ua.indexOf('msie '); IE 10 or older
        // const trident = ua.indexOf('trident/'); IE 11
        // const isFF = ua.indexOf('firefox') > -1;
        // const isEdge = ua.indexOf('edge') > -1

        return ua.indexOf(browser) > -1
    },

    isSafari() {
        const ua = window.navigator.userAgent.toLowerCase();
        return ua.indexOf('safari') > -1 && !(ua.indexOf('chrome') > -1);
    },

    compareTwoArray(source, target) {
        if (!source || !target) {
            return false
        }

        if (source.length !== target.length) {
            return false
        }
        const l = source.length;
        for (let i = 0; i < l; i++) {
            if (source[i] instanceof Array && target[i] instanceof Array) {
                if (!this.compareTwoArray(source[i], target[i])) {
                    return false
                }
            } else {
                if (source[i] != target[i]) {
                    return false
                }
            }
        }

        return true
    }
};

export default {
    modules,
}
